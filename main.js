function add() {
  // Também pode usar value para pegar o valor do campo
  // Também pode usar o id como seletor
  const newValue = parseInt(document.getElementsByClassName("add-text")[0].textContent) + 1;
  document.getElementsByClassName("add-text")[0].innerHTML = newValue;
}
